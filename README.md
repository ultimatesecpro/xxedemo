The project includes classes that demonstrates the <a rel="noopener noreferrer" href="https://www.owasp.org/index.php/Top_10-2017_A4-XML_External_Entities_(XXE)" target="_blank">OWASP A4 XML External Entities (XXE) attack</a>.

Created by <a rel="noopener noreferrer" href="www.linkedin.com/in/furmanmichael/" target="_blank">Michael Furman</a> and <a rel="noopener noreferrer" href="www.linkedin.com/in/anat-mazar" target="_blank">Anat Mazar</a>.

Read <a rel="noopener noreferrer" href="https://ultimatesecurity.pro/post/xxe-meetup/" target="_blank">the blog</a> for details.

