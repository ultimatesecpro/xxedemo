package org.ultimatesecpro.xxedemo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "xmlroot")
public class SimpleXmlEntry {

    private String xmlEntry;
    private SimpleXmlEntry() {
    }
    public String getXmlEntry() {
        return xmlEntry;
    }

}
