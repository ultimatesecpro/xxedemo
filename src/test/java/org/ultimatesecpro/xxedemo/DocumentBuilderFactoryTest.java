package org.ultimatesecpro.xxedemo;


import org.junit.Test;
import org.ultimatesecpro.xxeprotection.SafeDocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class DocumentBuilderFactoryTest {

    @Test
    public void testParse() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xmlroot><xmlEntry>3</xmlEntry></xmlroot>";
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));
        NodeList xmlEntry = doc.getElementsByTagName("xmlEntry");
        if (xmlEntry != null) {
            System.out.println(xmlEntry.item(0).getChildNodes().item(0).getNodeValue());
        }
    }

    @Test
    public void testXxeExternalCall() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "  <!ENTITY xxe SYSTEM \"http://api.geonames.org/timezoneJSON?lat=47.01&lng=10.2&username=ultimatesecpro\" >]><xmlroot><xmlEntry>&xxe;3</xmlEntry></xmlroot>";

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));
        NodeList xmlEntry = doc.getElementsByTagName("xmlEntry");
        if (xmlEntry != null) {
            System.out.println(xmlEntry.item(0).getChildNodes().item(0).getNodeValue());
        }
    }


    @Test
    public void testXxeReadFileLinux() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n " +
                "                      <!ELEMENT foo ANY >\n " +
                "                     <!ENTITY xxe SYSTEM \"file:///etc/passwd\" >]>" +
                "                    <xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));

        System.out.print(doc.getElementsByTagName("xmlroot").item(0).getTextContent());

    }

    @Test
    public void testXxeReadFileWin() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n " +
                "                      <!ELEMENT foo ANY >\n " +
                "                     <!ENTITY xxe SYSTEM \"file:////C://Windows//System32//drivers//etc//hosts\" >]>" +
                "                    <xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));

        System.out.print(doc.getElementsByTagName("xmlroot").item(0).getTextContent());

    }


    @Test
    public void testXxeSolvedSinglePlaceExternalCall() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "  <!ENTITY xxe SYSTEM \"http://api.geonames.org/timezoneJSON?lat=47.01&lng=10.2&username=ultimatesecpro\" >]><xmlroot><xmlEntry>&xxe;3</xmlEntry></xmlroot>";

        DocumentBuilder builder = SafeDocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));

        NodeList xmlEntry = doc.getElementsByTagName("xmlEntry");


        if (xmlEntry != null) {
            System.out.println(xmlEntry.item(0).getChildNodes().item(0).getNodeValue());
        }


    }


    @Test
    public void testXxeSolvedSinglePlaceReadFileLinux() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n " +
                "                      <!ELEMENT foo ANY >\n " +
                "                     <!ENTITY xxe SYSTEM \"file:///etc/passwd\" >]>" +
                "                    <xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";

        DocumentBuilder builder = SafeDocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));

        System.out.print(doc.getElementsByTagName("xmlroot").item(0).getTextContent());


    }

    @Test
    public void testXxeSolvedSinglePlaceReadFileWin() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n " +
                "                      <!ELEMENT foo ANY >\n " +
                "                     <!ENTITY xxe SYSTEM \"file:////C://Windows//System32//drivers//etc//hosts\" >]>" +
                "                    <xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";
        DocumentBuilder builder = SafeDocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));

        System.out.print(doc.getElementsByTagName("xmlroot").item(0).getTextContent());

    }

}