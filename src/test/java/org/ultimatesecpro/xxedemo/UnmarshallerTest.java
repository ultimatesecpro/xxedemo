package org.ultimatesecpro.xxedemo;

import org.junit.Test;
import org.ultimatesecpro.xxeprotection.SafeSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;


public class UnmarshallerTest {

    @Test
    public void testSimpleXml() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xmlroot><xmlEntry>3</xmlEntry></xmlroot>";
        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(new StringReader(xml));
        assertEquals("3", simpleXmlEntry.getXmlEntry());
        System.out.println(simpleXmlEntry.getXmlEntry());

    }

    @Test
    public void testSimpleXmlWithEntities() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "<!ENTITY message \"Wakanda Forever!\">\n" +
                "]><xmlroot><xmlEntry>&message;</xmlEntry></xmlroot>";

        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(simpleXmlEntry.getXmlEntry());
    }

    @Test
    public void testXxeExternalCall() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "  <!ENTITY xxeprotection SYSTEM \" http://api.geonames.org/timezoneJSON?lat=47.01&lng=10.2&username=ultimatesecpro\" >]><xmlroot><xmlEntry>&xxeprotection;</xmlEntry></xmlroot>";
        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(simpleXmlEntry.getXmlEntry());
    }


    @Test
    public void testXxeReadFileWin() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "<!ENTITY xxe SYSTEM \"file:////C://Windows//System32//drivers//etc//hosts\">\n" +
                "]><xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";

        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(simpleXmlEntry.getXmlEntry());


    }


    @Test
    public void testXxeReadFileLinux() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "<!ENTITY xxe SYSTEM \"file:///etc/passwd\">\n" +
                "]><xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";

        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(simpleXmlEntry.getXmlEntry());


    }


    @Test
    public void testXxeSolvedSinglePlaceExternalCall() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "  <!ENTITY xxeprotection SYSTEM \" http://api.geonames.org/timezoneJSON?lat=47.01&lng=10.2&username=ultimatesecpro\" >]><xmlroot><xmlEntry>&xxeprotection;</xmlEntry></xmlroot>";


        Source xmlSource = SafeSource.newInstanceFromXmlContent(xml);
        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(xmlSource);
        System.out.println(simpleXmlEntry.getXmlEntry());

    }


    @Test
    public void testXxeSolvedSinglePlaceReadFileLinux() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "<!ENTITY xxe SYSTEM \"file:///etc/passwd\">\n" +
                "]><xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";


        Source xmlSource = SafeSource.newInstanceFromXmlContent(xml);

        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();
        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(xmlSource);
        System.out.println(simpleXmlEntry.getXmlEntry());

    }

    @Test
    public void testXxeSolvedSinglePlaceReadFileWin() throws Exception {

        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ \n" +
                "  <!ELEMENT foo ANY >\n" +
                "<!ENTITY xxe SYSTEM \"file:////C://Windows//System32//drivers//etc//hosts\">\n" +
                "]><xmlroot><xmlEntry>&xxe;</xmlEntry></xmlroot>";

        Source xmlSource = SafeSource.newInstanceFromXmlContent(xml);
        final Unmarshaller unmarshaller = JAXBContext.newInstance(SimpleXmlEntry.class).createUnmarshaller();

        final SimpleXmlEntry simpleXmlEntry = (SimpleXmlEntry) unmarshaller.unmarshal(xmlSource);
        System.out.println(simpleXmlEntry.getXmlEntry());

    }

}